var gulp = require('gulp');
var inject = require("gulp-inject");
var config = require('../config').releaseindex;                                                         

gulp.task('releaseindex', ['releasecopyindex'], function () {
  
  var target = gulp.src(config.src);
  var sources = gulp.src(config.sources, {read: false});

  return target.pipe(inject(sources, config.options))
    .pipe(gulp.dest(config.dest));
});