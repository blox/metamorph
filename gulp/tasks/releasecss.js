var gulp = require('gulp');
var minifyCSS = require('gulp-minify-css');
var config = require('../config').releasecss;

gulp.task('releasecss', ['sass'], function () {
  return gulp.src(config.src)
    .pipe(minifyCSS({keepBreaks:true}))
    .pipe(gulp.dest(config.dest));
});