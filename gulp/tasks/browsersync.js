var browserSync = require('browser-sync');
var gulp = require('gulp');
var config = require('../config').browsersync;

gulp.task('browsersync', ['build'], function() {
  browserSync(config);
});
