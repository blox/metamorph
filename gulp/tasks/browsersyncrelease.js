var browserSync = require('browser-sync');
var gulp = require('gulp');
var config = require('../config').browsersyncrelease;

gulp.task('browsersyncrelease', ['release'], function() {
  browserSync(config);
});
