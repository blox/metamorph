var gulp = require('gulp');
var clean = require('gulp-clean');
var config = require('../config').releasecleanindex;

gulp.task('releasecleanindex', function () {
  return gulp.src(config.src)
    .pipe(clean());
});
