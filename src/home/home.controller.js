angular.module('home.controller', [])

.controller('home.controller', function($scope, escapemapData) {

  $scope.escapemapData = escapemapData;
  
  $scope.randomData = function(){
    var obj = {};
 
    function rInt(min, max) {
      return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    obj.map = {
      height:rInt(200,500),
      width:rInt(200,1000),
      wallWidth:rInt(5,20),
      crewOffset:rInt(20,50),
      shipOffset:rInt(20,50),
      doorOffset:rInt(4,4),
      pathOffset:rInt(2,2)        
    };
    
    var walls =[];
    
    //50 px min reserved on each side of map.
    //10px min in between each wall.
    
    var maxNumWalls = (obj.map.width - 100)/(obj.map.wallWidth + 10);
    var numWalls = rInt(2, maxNumWalls);
    var wallPadding = (obj.map.width - 100)/(numWalls);
    var wallXOffset = 50+(wallPadding/2);

    
    for(var i = 0;i<numWalls;i++){
      var numDoors = rInt(4,8);
      var wallYOffset = 0;
      
      for(var j = 0;j<numDoors;j++){
        var fragments = [];
        var randomOffset = rInt(10,20);
        var f1Y = j==0?0:wallYOffset+randomOffset;
        var f2Y = wallYOffset + ((obj.map.height/numDoors));
        
        var fragment1 = {
          x:wallXOffset,
          y:f1Y
        };
 
        var fragment2 = {
          x:wallXOffset,
          y:f2Y
        };
        
        if(j+1==numDoors){
          fragment2.y = obj.map.height;
        }
        
        wallYOffset = wallYOffset + ((obj.map.height/numDoors))+randomOffset;


        fragments.push(fragment1);
        fragments.push(fragment2);
        walls.push(fragments);
      }
      
      wallXOffset = wallXOffset + wallPadding;
      
    }


    
    var crew = [];
    var crewYOffset = 0;
    var crewOffset = obj.map.crewOffset;
    var mapHeight = obj.map.height;
    var races = ['zerg','protoss','terran'];
    var colorMap = {
      'zerg':'rgb(200,200,122)',
      'protoss':'rgb(123,71,60)',
      'terran':'rgb(40,96,145)'
    };
    
    
    while(mapHeight-crewYOffset>50){
      var radius = rInt(4,8);
      var race = races[rInt(0,2)];
      var color = colorMap[race];
      var pathSize = rInt(2,4);
      
      var member = {
        r: radius,
        t: race,
        c: color,
        s: pathSize
      };
      crew.push(member);
      crewYOffset = crewYOffset+(radius*2)+crewOffset;
    }




    var ships = [];
    var shipYOffset = 0;
    var shipOffset = obj.map.shipOffset;
    var races = ['zerg','protoss','terran'];
    var colorMap = {
      'zerg':'rgb(200,200,122)',
      'protoss':'rgb(123,71,60)',
      'terran':'rgb(40,96,145)'
    };
    
    
    while(mapHeight-shipYOffset>50){
      var radius = rInt(4,20);
      var race = races[rInt(0,2)];
      var color = colorMap[race];
      
      var ship = {
        r: radius,
        t: race,
        c: color
      };
      ships.push(ship);
      shipYOffset = shipYOffset+(radius*2)+shipOffset;
    }
    
    
    obj.walls = walls;
    obj.crew = crew;
    obj.ships = ships;
    
    $scope.escapemapData = obj;
  }
});